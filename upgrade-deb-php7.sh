echo 'deb http://packages.dotdeb.org jessie all'>>/etc/apt/sources.list
echo 'deb-src http://packages.dotdeb.org jessie all' >> /etc/apt/sources.list
wget http://www.dotdeb.org/dotdeb.gpg
cat dotdeb.gpg | apt-key add -
apt-get update
apt-get install libapache2-mod-php7.0 php7.0-apcu php7.0-apcu-bc php7.0-cli php7.0-common php7.0-curl php7.0-gd php7.0-igbinary php7.0-imagick php7.0-imap php7.0-intl php7.0-json php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-opcache php7.0-phpdbg php7.0-readline php7.0-redis php7.0-sqlite3 php7.0-xml php7.0-zip
a2dismod php5
a2enmod php7.0
service apache2 restart
